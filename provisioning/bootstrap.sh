#!/bin/sh

## Install .net core
echo Installing .net core

### Add the dotnet apt-get feed
echo Adding the dotnet apt-get feed
sudo sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet-release/ xenial main" > /etc/apt/sources.list.d/dotnetdev.list'
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 417A0893
sudo apt-get update

### Install .net core SDK
echo Installing the .net core SDK
sudo apt-get install -y dotnet-dev-1.0.1


## Install docker
echo Installing Docker

### Set up the repository
echo Setting up the repository
sudo apt-get -y install \
  apt-transport-https \
  ca-certificates \
  curl

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
sudo apt-get update

### Get Docker CE
echo Getting Docker CE
sudo apt-get -y install docker-ce

### (Optional) Add current user to the `docker` group
sudo usermod -a -G docker $(whoami)

